package com.devalgas.lambdaexpressionstreamstraining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LambdaExpressionStreamsTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(LambdaExpressionStreamsTrainingApplication.class, args);
	}

}
