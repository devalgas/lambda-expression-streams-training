package com.devalgas.lambdaexpressionstreamstraining.entities;

import lombok.ToString;

@ToString
public class Person {

    private String name;
    private int age;
    private String city;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Person(String name, int age, String city) {
        this.name = name;
        this.age = age;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

}
