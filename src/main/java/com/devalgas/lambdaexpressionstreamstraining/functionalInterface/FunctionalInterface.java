package com.devalgas.lambdaexpressionstreamstraining.functionalInterface;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;
import java.util.List;
import java.util.function.*;

@SpringBootApplication
public class FunctionalInterface implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(FunctionalInterface.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

// standard

        // crée une fonction qui retourne un nombre entier aléatoire entre 0 et 99
        Supplier<Integer> randomNumber = () -> (int) (Math.random() * 100);

        // appel de la méthode get() de la fonction pour obtenir un nombre aléatoire
        int number = randomNumber.get();

        System.out.println("Nombre aléatoire : " + number);

        List<String> names = Arrays.asList("Amadou", "Ulrich", "Saahir", "Devalgas");

        // crée une fonction qui prend une chaîne de caractères en entrée et l'affiche à la console
        Consumer<String> printName = name -> System.out.println(name);

		// appel de la méthode forEach() de la liste pour appliquer la fonction à chaque élément de la liste
        names.forEach(printName);

        // crée une fonction qui prend une chaîne de caractères en entrée et retourne sa longueur
        Function<String, Integer> stringLength = str -> str.length();

        // appel de la méthode apply() de la fonction pour obtenir la longueur de la chaîne de caractères
        int length = stringLength.apply("Hello world");

        System.out.println("Longueur de la chaîne de caractères : " + length);

        // création d'une instance de la fonction Predicate qui vérifie si un nombre est pair
        Predicate<Integer> isEven = numberPredicate -> number % 2 == 0;

		// appel de la fonction Predicate pour vérifier si un nombre est pair
        boolean result = isEven.test(42);

        System.out.println(result);

		// création d'une instance de la fonction UnaryOperator qui multiplie un nombre par 2
        UnaryOperator<Integer> doubleNumber = numberUnaryOperator -> number * 2;

		// Appel de la fonction UnaryOperator pour multiplier un nombre par 2
        Integer resultUnaryOperator = doubleNumber.apply(42);

        System.out.println(resultUnaryOperator);

        // création d'une instance de la fonction BinaryOperator qui calcule la somme de deux nombres
        BinaryOperator<Integer> addNumbers = (a, b) -> a + b;

        // Appel de la fonction BinaryOperator pour calculer la somme de deux nombres
        Integer resultBinaryOperator = addNumbers.apply(40, 2);

        System.out.println(resultBinaryOperator);

        // création d'une instance de la fonction BiFunction qui concatène deux Strings
        BiFunction<String, String, String> concatenateStrings = (str1, str2) -> str1 + str2;

        // appel de la fonction BiFunction pour concaténer deux Strings
        String resultBiFunction = concatenateStrings.apply("Hello, World!", " How are you?");

        System.out.println(result);

// spécialisées

		// déclaration d'une fonction IntFunction<R> qui prend un entier en entrée et renvoie une chaîne de caractères en sortie
		IntFunction<String> convertIntToString = x -> String.valueOf(x);

        // utilisation de la fonction IntFunction<R> pour convertir un entier en chaîne de caractères
		String str = convertIntToString.apply(42);

        // déclaration d'une fonction LongFunction<R> qui prend un long en entrée et renvoie une chaîne de caractères en sortie
		LongFunction<String> convertLongToString = x -> String.valueOf(x);

        // utilisation de la fonction LongFunction<R> pour convertir un long en chaîne de caractères
		String strLongFunction = convertLongToString.apply(123456789L);

        // déclaration d'une fonction DoubleFunction<R> qui prend un double en entrée et renvoie une chaîne de caractères en sortie
		DoubleFunction<String> convertDoubleToString = x -> String.format("%.2f", x);

		// utilisation de la fonction DoubleFunction<R> pour convertir un double en chaîne de caractères
		String strDoubleFunction = convertDoubleToString.apply(1234.56789);

        // déclaration d'une fonction IntUnaryOperator qui prend un entier en entrée et renvoie son carré en sortie
		IntUnaryOperator square = x -> x * x;

        // utilisation de la fonction IntUnaryOperator pour calculer le carré d'un entier
		int resultIntUnaryOperator = square.applyAsInt(5);

        // déclaration d'une fonction LongUnaryOperator qui prend un long en entrée et renvoie son carré en sortie
		LongUnaryOperator squareLongUnaryOperator = x -> x * x;

        // utilisation de la fonction LongUnaryOperator pour calculer le carré d'un long
		long resultLongUnaryOperator = squareLongUnaryOperator.applyAsLong(5L);

        // déclaration d'une fonction DoubleUnaryOperator qui prend un double en entrée et renvoie sa valeur absolue en sortie
	 	DoubleUnaryOperator absolute = x -> Math.abs(x);

        // Utilisation de la fonction DoubleUnaryOperator pour calculer la valeur absolue d'un double
		double resultDoubleUnaryOperator = absolute.applyAsDouble(-3.14);

        // déclaration d'une fonction IntBinaryOperator qui prend deux entiers en entrée et renvoie leur somme en sortie
		IntBinaryOperator sum = (x, y) -> x + y;

        // utilisation de la fonction IntBinaryOperator pour calculer la somme de deux entiers
		int resultIntBinaryOperator = sum.applyAsInt(3, 5);

        // déclaration d'une fonction LongBinaryOperator qui prend deux long en entrée et renvoie leur somme en sortie
		LongBinaryOperator additionLongBinaryOperator = (x, y) -> x + y;

        // Utilisation de la fonction LongBinaryOperator pour calculer la somme de deux long
		long resultLongBinaryOperator = additionLongBinaryOperator.applyAsLong(10L, 20L);

        // déclaration d'une fonction DoubleBinaryOperator qui prend deux double en entrée et renvoie leur produit en sortie
		DoubleBinaryOperator multiplication = (x, y) -> x * y;

        // utilisation de la fonction DoubleBinaryOperator pour calculer le produit de deux double
		double resultDoubleBinaryOperator = multiplication.applyAsDouble(2.5, 3.0);


// composition fonctionnelle

		// - compose

		Function<Integer, Integer> addFive = x -> x + 5;

        // la fonction multiplyByTwo multiplie un entier par 2
		Function<Integer, Integer> multiplyByTwo = x -> x * 2;

		// La fonction multiplyByTwoAndAddFive multiplie l'entier d'entrée par 2, puis ajoute 5 au résultat
		Function<Integer, Integer> multiplyByTwoAndAddFive = addFive.compose(multiplyByTwo);

		// - andThen

		Function<Integer, Integer> squareCompose = x -> x * x;


        // la fonction toString crée une chaîne de caractères pour afficher ce carré
		Function<Integer, String> toString = x -> "Le carré de " + x + " est " + x * x;

        // la fonction squareToStringcalcule d'abord le carré de l'entrée, puis crée la chaîne de caractères correspondante
		Function<Integer, String> squareToString = squareCompose.andThen(toString);

        // utilisation de la fonction squareToStringcalcule
		String resultSquareToString = squareToString.apply(5);
    }
}
