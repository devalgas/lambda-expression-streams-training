package com.devalgas.lambdaexpressionstreamstraining.lambdaExpressions;

import com.devalgas.lambdaexpressionstreamstraining.entities.Person;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@SpringBootApplication
public class LambdaExpressions implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(LambdaExpressions.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

// lambda expression

        // simple : (x, y) ->  x * y;


        // avec functional interface : MyFunction multiply = (a, b) -> a * b;


		// avec des streams
        List<String> names = Arrays.asList("Amadou", "Ulrich", "Saahir", "Devalgas");
        List<String> filteredNames = names.stream()
                .filter(name -> name.startsWith("A")).toList();

// capturing
        int x = 5;
        Runnable r = () -> System.out.println("x = " + x);
        r.run();

// interaction avec les expressions

        // - collections
        List<String> namesInteraction = Arrays.asList("Amadou", "Ulrich", "Saahir", "Devalgas");
        namesInteraction.forEach(System.out::println);

        // - functional interface
        List<Person> people = Arrays.asList(
                new Person("Alice", 25),
                new Person("Bob", 30),
                new Person("Charlie", 20)
        );
        Collections.sort(people, Comparator.comparing(Person::getAge));

        // - streams
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        List<Integer> evenNumbers = numbers.stream()
                .filter(n -> n % 2 == 0)
                .collect(Collectors.toList());

// Méthodes de Références

        // - statique : Integer::parseInt;
        Function<String, Integer> parseInt = Integer::parseInt;

        // - instance : System.out::println
        names.forEach(System.out::println);

        //- constructeur de classe : ArrayList::new
        Supplier<List<String>> listSupplier = ArrayList::new;
        List<String> namesListSupplier = listSupplier.get();

        // - constructeur de tableau : int[]::new
        Function<Integer, int[]> intArrayCreator = int[]::new;
        int[] numbersIntArrayCreator = intArrayCreator.apply(10);
    }

}
