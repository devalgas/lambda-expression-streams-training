package com.devalgas.lambdaexpressionstreamstraining.streams;

import com.devalgas.lambdaexpressionstreamstraining.entities.Person;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

@SpringBootApplication
public class Streams implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Streams.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

// différents types de streams

		// - stream
		// création d'un stream à partir de la liste
		List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
		List<Integer> evenNumbers = numbers.stream().filter(n -> n % 2 == 0)
				.collect(Collectors.toList());

		// - ParallelStream : création d'une liste de nombres entiers et que nous voulions les trier en ordre décroissant p
		List<Integer> numbersParallelStream = Arrays.asList(1, 5, 3, 7, 2, 8, 4, 9, 6);
		List<Integer> sortedNumbers = numbersParallelStream.parallelStream()
				.sorted(Comparator.reverseOrder())
				.collect(Collectors.toList());

		// - spécialisées
		int[] intArray = {1, 2, 3, 4, 5};
		long[] longArray = {10L, 20L, 30L, 40L, 50L};
		double[] doubleArray = {0.5, 1.0, 1.5, 2.0, 2.5};
        // somme des éléments du tableau d'entiers
		int sumInt = IntStream.of(intArray).sum();
		// somme des éléments du tableau de longs
		long sumLong = LongStream.of(longArray).sum();
		// somme des éléments du tableau de doubles
		double sumDouble = DoubleStream.of(doubleArray).sum();

		// - baseStream
		// création un de nombres entiers de 1 à 9 et filtre les entiers pairs avec filter et et affiche chaque entier restant avec forEach
		IntStream.range(1, 10)
				.filter(n -> n % 2 == 0)
				.forEach(System.out::println);

// Opérations streams

		// - intermédiaires

		// création d'une liste de nombres
		List<Integer> numbersOperations = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        // filtrage des nombres impairs
		List<Integer> oddNumbers = numbersOperations.stream()
				.filter(n -> n % 2 != 0)
				.toList();

		// transformation des nombres pairs en leur carré
		List<Integer> evenSquares = numbers.stream()
				.filter(n -> n % 2 == 0)
				.map(n -> n * n)
				.toList();

        // concaténation de deux listes
		List<String> firstList = Arrays.asList("foo", "bar");
		List<String> secondList = Arrays.asList("baz", "qux");
		List<String> combinedList = firstList.stream()
				.flatMap(s1 -> secondList.stream().map(s2 -> s1 + s2))
				.toList();

        // Élimination des doublons
		List<Integer> duplicateNumbers = Arrays.asList(1, 2, 3, 1, 2, 3, 4, 5);
		List<Integer> distinctNumbers = duplicateNumbers.stream()
				.distinct()
				.toList();

        // Tri des nombres par ordre décroissant
		List<Integer> unsortedNumbers = Arrays.asList(3, 6, 1, 8, 2, 5);
		List<Integer> numbersSorted = unsortedNumbers.stream()
				.sorted((n1, n2) -> n2 - n1)
				.toList();

        // Limitation du nombre de résultats
		List<Integer> limitedNumbers = numbers.stream()
				.limit(5)
				.toList();

        // ignorer les premiers résultats
		List<Integer> skippedNumbers = numbers.stream()
				.skip(7)
				.toList();


//	finales

		// création d'une liste de strings
		List<String> fruits = Arrays.asList("pomme", "banane", "cerise", "pomme", "orange");

        // utilisation de la méthode forEach pour afficher chaque élément du stream
		fruits.stream().forEach(System.out::println);

        // utilisation de la méthode toArray pour convertir le stream en un tableau
		String[] fruitsArray = fruits.stream().toArray(String[]::new);

        // utilisation de la méthode reduce pour obtenir la concaténation de tous les éléments du stream
		String concatenated = fruits.stream().reduce("", (s1, s2) -> s1 + s2);

        // Utilisation de la méthode collect pour obtenir une liste des éléments distincts du stream
		List<String> distinctFruits = fruits.stream().distinct().collect(Collectors.toList());

        // utilisation de la méthode count pour obtenir le nombre d'éléments dans le stream
		long count = fruits.stream().count();

        // utilisation de la méthode limit pour limiter le nombre d'éléments dans le stream
		List<String> limitedFruits = fruits.stream().limit(3).collect(Collectors.toList());


        // utilisation de la méthode allMatch pour vérifier si tous les éléments du stream commencent par "p"
		boolean allStartWithP = fruits.stream().allMatch(s -> s.startsWith("p"));

        // utilisation de la méthode noneMatch pour vérifier si aucun élément du stream ne contient "kiwi"
		boolean noneContainsKiwi = fruits.stream().noneMatch(s -> s.contains("kiwi"));

        // utilisation de la méthode findFirst pour obtenir le premier élément du stream
		String firstFruit = fruits.stream().findFirst().orElse("");


		// utilisation de la méthode findAny pour obtenir n'importe quel élément du stream
		String anyFruit = fruits.stream().findAny().orElse("");


// Fonctions Collector

		// création d'une liste de personnes
		List<Person> persons = Arrays.asList(
				new Person("Alice", 25),
				new Person("Bob", 30),
				new Person("Charlie", 35),
				new Person("Dave", 40),
				new Person("Eve", 45)
		);

        // utilisation de la fonction toList pour créer une liste à partir du stream
		List<String> names = persons.stream()
				.map(Person::getName)
				.collect(Collectors.toList());

        // utilisation de la fonction toSet pour créer un ensemble à partir du stream
		Set<Integer> ages = persons.stream()
				.map(Person::getAge)
				.collect(Collectors.toSet());

        // utilisation de la fonction toMap pour créer une carte à partir du stream
		Map<String, Integer> nameAgeMap = persons.stream()
				.collect(Collectors.toMap(Person::getName, Person::getAge));

        // utilisation de la fonction joining pour joindre les noms dans une chaîne
		String nameString = persons.stream()
				.map(Person::getName)
				.collect(Collectors.joining(", "));

        // utilisation de la fonction counting pour compter le nombre de personnes dans le stream
		long counting = persons.stream()
				.collect(Collectors.counting());

		// utilisation de la fonction averagingInt pour calculer la moyenne des âges
		double averageAge = persons.stream()
				.collect(Collectors.averagingInt(Person::getAge));

        // utilisation de la fonction summingInt pour calculer la somme des âges
		int totalAge = persons.stream()
				.collect(Collectors.summingInt(Person::getAge));

        // utilisation de la fonction maxBy pour trouver la personne la plus âgée
		Optional<Person> oldestPerson = persons.stream()
				.collect(Collectors.maxBy(Comparator.comparing(Person::getAge)));

        // utilisation de la fonction minBy pour trouver la personne la plus jeune
		Optional<Person> youngestPerson = persons.stream()
				.collect(Collectors.minBy(Comparator.comparing(Person::getAge)));

	}
}
